# -*- coding: utf-8 -*-
""" scinstr.tctrl.constants.py """

# Default Cryo-Con 24C device parameters
CC24C_ID = "Cryo-con,24C"
CC24C_PORT = 5000
CC24C_DEFAULT_TIMEOUT = 2.0
