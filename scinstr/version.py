# -*- coding: utf-8 -*-
# scinstr/version.py

__version__ = "0.3.4"

# 0.3.4  (15/02/2022): lakeshore-utils: add capability to download/upload
#                      a device configuration. Add support of USB interface.
#                      rawto340: add selection of column of input file.
#                      gen340file: add selection of columns (temperature,
#                      sensor value) of input file.
#                      extract-peak: update PyQt depence from v4 to v5.
#                      l350.py: upate R/W methods and add list of whole
#                      'SCPI' commands.
#                      setup.py: correct/update scripts and dependencies.
# 0.3.3  (06/10/2021): Correct problem of subpackage not added.
# 0.3.2  (30/06/2021): Add scripts
# 0.3.1  (21/04/2021): Add new modules in subpackage tctrl, vna and .
#                      Add new subpackage phimeter
# 0.3.0  (06/03/2020): - Divide package into subpackage.
#                      - Name of signals use snake case instead of camel case.
# 0.2.0  (13/12/2019): Add support of signal/slot facilities in pure python
#                      using signalslot package.
# 0.1.3  (01/10/2019): Update daq34972a.py to PyQt5.
# 0.1.2  (19/03/2019): Move to PyQt5
# 0.1.1  (14/03/2019): Lot of modifications...
# 0.1.0a0 (18/09/2018): Initial version
